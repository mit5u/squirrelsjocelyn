data_act_squirrels <- read_csv("~/data/nyc_squirrels_act_sample.csv") %>%
  head(15)

usethis::use_data(data_act_squirrels,
                  overwrite = TRUE)

cat(sinew::makeOxygen("data_act_squirrels"),
    file = "R/doc_data_act_squirrels.R")
rstudioapi::navigateToFile("R/doc_data_act_squirrels.R")
