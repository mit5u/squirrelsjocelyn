#' @title data_act_squirrels
#' @description Data about age, fur color and activity of squirrels in Central Park
#' @format A data frame with 15 rows and 4 variables:
#' \describe{
#'   \item{\code{age}}{character Age of the squirrels}
#'   \item{\code{primary_fur_color}}{character Main color of the fur}
#'   \item{\code{activity}}{character Main activity}
#'   \item{\code{counts}}{double Number of squirrels}
#'}
#' @details Sample data from the ThinkR Package Course
"data_act_squirrels"